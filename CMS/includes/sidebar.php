
<div class="col-lg-4">
                    <!-- Search widget-->
                    <div class="card mb-4">
                        <div class="card-header">Search</div>
                        <div class="card-body">
                            <form class="input-group" action="search.php" method="post">
                                <input class="form-control" type="text"  autocomplete="off" name="search" placeholder="Enter search term..." aria-label="Enter search term..." aria-describedby="button-search" />
                                <button class="btn btn-primary" id="button-search" name="submit" type="submit">Go!</button>
                        </form>
                        </div>
                    </div>
                    <!-- Categories widget-->
                    <div class="card mb-4">
                        <div class="card-header">Categories</div>
                        <div class="card-body">
                            <div class="row">
                                    <ul class="list-unstyled mb-0">
                                    <?php
                               
                                    $select_all_category = "SELECT * FROM category";
                                    $side_cat_result = mysqli_query($connection , $select_all_category);
  
  
                                    while($row = mysqli_fetch_assoc($side_cat_result)){
                                    $cat_title = $row['category_title'];
                                    $cat_id = $row['category_id'];
                                    echo "<li><a href='category.php?id={$cat_id}'>{$cat_title}</a></li>";
                                    }
                                    ?>
                                    </ul>
                                <!-- <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">JavaScript</a></li>
                                        <li><a href="#!">CSS</a></li>
                                        <li><a href="#!">Tutorials</a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- Side widget-->
                    <div class="card mb-4">
                        <div class="card-header">Popular Posts</div>

                        <?php
                        
                        $pop_post_query = "SELECT * FROM posts WHERE post_status = 'Published'";
                        $pop_post_result = mysqli_query($connection , $pop_post_query);

                        while($row = mysqli_fetch_assoc($pop_post_result)){
                            $post_id = $row['post_id'];
                            $post_title = $row['post_title'];
                            $post_date = $row['post_date'];
                            $post_image = $row['post_image'];
                        ?>
                        <div class="card-body d-flex flex-row">
                            <div>
                                <img src="./img/<?php echo $post_image;?>" alt="" class="img-fluid" width="150px">
                            </div>
                            <div class="p-2">
                                <a href="blog_details.php?post=<?php echo $post_id; ?>" style="text-decoration: none; color:black;">
                                    <h5><?php echo $post_title;?></h5>
                                </a>
                                <p class="text-uppercase"><?php echo $post_date;?></p>
                            </div>
                        </div>
                            <?php } ?>
                    </div>
                </div>
            </div>
        </div>