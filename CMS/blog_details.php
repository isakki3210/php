<?php
include './includes/db.php';
include './includes/header.php';
?>
<div class="container mt-5">
            <div class="row">
                <div class="col-lg-8">
                    
                <?php

                    if(isset($_GET['post'])){
                        $post_id = $_GET['post'];
                    }

                    $post_query = "SELECT * FROM posts WHERE post_id = $post_id";
                    $post_result = mysqli_query($connection , $post_query);

                    while($row = mysqli_fetch_assoc($post_result)){
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_content = $row['post_content'];
                        $post_image = $row['post_image'];
                        $post_tag = $row['post_tags'];
                    }
                    ?>
                <article>
                        <!-- Post header-->
                        <header class="mb-4">
                            <!-- Post title-->
                            <h1 class="fw-bolder mb-1"><?php echo $post_title;?></h1>
                            <!-- Post meta content-->
                            <div class="text-muted fst-italic mb-2">Posted on <?php echo $post_date;?> by <?php echo $post_author;?></div>
                            <!-- Post categories-->
                            <a class="badge bg-secondary text-decoration-none link-light" href="*"><?php echo $post_tag;?></a>
                        </header>
                        <!-- Preview image figure-->
                        <figure class="mb-4"><img class="img-fluid rounded" src="./img/<?php echo $post_image;?>" alt="..." /></figure>
                        <!-- Post content-->
                        <section class="mb-5">
                            <p class="fs-5 mb-4"><?php echo $post_content;?></p>
                        </section>
                    </article>
</div>


                <section class="mb-5 container">
                            <?php
                               $select_all_query = "SELECT * FROM comments WHERE comment_post_id = $post_id AND comment_status = 'Approved'";
                               $select_all_result = mysqli_query($connection,$select_all_query);
                               while($row = mysqli_fetch_assoc($select_all_result)){                                       
                                   $comment_author = $row['comment_author'];
                                   $comment_content = $row['comment_content'];
                                   $comment_date = $row['comment_date'];               
                            ?>
                        <div class="card bg-light">

     
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                    <div class="ms-3">
                                        <div class="fw-bold"><?php echo $comment_author?></div>
                                        <p><?php echo $comment_date?></p>
                                        <p>
                                        <?php echo $comment_content?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>

                    </section>



                    


                </div>
        <div class="comment-form container">
            <h4>Leave a Comment</h4>
            <form action="blog_details.php?post=<?php echo $post_id?>" method="post">
            <div class="form-group from-inline">
            </div>
            <div class="form-group">
                <input type="text" class="form-control mb-10" name="author" placeholder="Name" autocomplete="off"><br>
                <input type="email" class="form-control mb-10" name="email" placeholder="Email"  autocomplete="off"><br>
                <textarea class="form-control mb-10" rows="5" name="message" placeholder="Message" required="" id="" cols="30" ></textarea><br>
            </div>
            <input type="submit" value="Send Message" name="create_comment" class="btn btn-primary submit_btn">
        </form><br>
        </div>
    </div>
</div>



<?php include 'includes/footer.php';?>



<?php
                //   sending commentd to the database 

if(isset($_POST['create_comment'])){
    $comment_author = $_POST['author'];
    $comment_email = $_POST['email'];
    $comment_message = $_POST['message'];

    $comment_query = "INSERT INTO comments (comment_post_id , comment_author , comment_email , comment_content , comment_date , comment_status ) VALUES ($post_id , '$comment_author' , '$comment_email' , '$comment_message' , now() , 'UnApproved')";
    $comment_result = mysqli_query($connection , $comment_query);
}

?>