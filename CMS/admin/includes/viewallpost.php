<?php ob_start();

if (isset($_POST['selectBox'])) {
    foreach ($_POST['selectBox'] as $postIdvalue) {
        $select_option = $_POST['select_option'];
        switch ($select_option) {
            case 'Published';
                $published_query = "UPDATE posts SET post_status = '$select_option' WHERE post_id = $postIdvalue";
                $published_result = mysqli_query($connection, $published_query);
                break;

            case 'UnPublished';
                $unpublished_query = "UPDATE posts SET post_status = '$select_option' WHERE post_id = $postIdvalue";
                $unpublished_result = mysqli_query($connection, $unpublished_query);
                break;
        }
    }
}

?>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">View All Posts</h6>
    </div>
    <div class="card-body">
        <form action="post.php" method="post">
            <div class="row">
                <div class="col-xl-4" id="bulkOption">
                    <select name="select_option" class="form-control">
                        <option value="Published">Publish</option>
                        <option value="UnPublished">UnPublish</option>
                    </select>
                </div>
                <div class="col-xl-4">
                    <input type="submit" class="btn btn-sm btn-primary shadow-sm" value="Apply">
                </div>
            </div>
            <br>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col"><input type="checkbox" id="selectAllBox"></th>
                        <th scope="col">Author</th>
                        <th scope="col">Title</th>
                        <th scope="col">Category</th>
                        <th scope="col">Status</th>
                        <th scope="col">Image</th>
                        <th scope="col">Tags</th>
                        <th scope="col">Comments</th>
                        <th scope="col">Date</th>
                        <th scope="col">Publish</th>
                        <th scope="col">Unpublish</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    $select_all_query = "SELECT * FROM posts";
                    $select_all_result = mysqli_query($connection, $select_all_query);

                    while ($row = mysqli_fetch_assoc($select_all_result)) {
                        $post_title = $row['post_title'];
                        $post_id = $row['post_id'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_content = $row['post_content'];
                        $post_image = $row['post_image'];
                        $post_tags = $row['post_tags'];
                        $post_comment_count = $row['post_comment_count'];
                        $post_category_id = $row['post_category_id'];
                        $post_status = $row['post_status'];


                        echo "<tr>";
                    ?>
                        <td><input type="checkbox" class="checkboxs" name="selectBox[]" value="<?php echo $post_id ?>"></td>
                        <?php

                        echo "
                    <td>{$post_author}</td>
                    <td>{$post_title}</td>";

                        $select_all_cat_query = "SELECT * FROM category WHERE category_id = $post_category_id";
                        $select_all_cat_result = mysqli_query($connection, $select_all_cat_query);

                        while ($row = mysqli_fetch_assoc($select_all_cat_result)) {
                            $cat_title = $row['category_title'];


                            echo "<td>{$cat_title}</td>";
                        }



                        echo "
                    <td>{$post_status}</td>
                    <td><img src='../img/$post_image' alt='' class='img-fluid' width='100px'></td>
                    <td>{$post_tags}</td> ";


                        $comment_count_query = "SELECT * FROM comments WHERE comment_post_id = $post_id AND comment_status = 'Approved'";
                        $comment_count_result = mysqli_query($connection, $comment_count_query);
                        $comment_count = mysqli_num_rows($comment_count_result);

                        echo "   <td>{$comment_count}</td> ";

                        echo "  <td>{$post_date}</td>
                            <td><a href='post.php?publish={$post_id}' class='btn btn-outline-success'>publish</a></td>
                            <td><a href='post.php?unpublish={$post_id}' class='btn btn-outline-danger'>unpublish</a></td>
                            <td><a href='post.php?page=edit_post&post_id={$post_id}' class='btn btn-outline-warning'>Edit</a></td>
                            <td><a href='post.php?delete={$post_id}' class='btn btn-outline-danger'>Delete</a></td>
                            </tr>"

                        ?>

                    <?php } ?>


                </tbody>
            </table>

        </form>
    </div>
</div>


<?php
if (isset($_GET['delete'])) {
    $delete_post_id = $_GET['delete'];
    $delete_post_query = "DELETE FROM posts WHERE post_id = $delete_post_id";
    $delete_result = mysqli_query($connection, $delete_post_query);
    header('location: post.php');
}




if (isset($_GET['publish'])) {
    $publish_post_id = $_GET['publish'];
    $publish_post_query = "UPDATE posts SET post_status = 'Published' WHERE post_id = $publish_post_id";
    $publish_result = mysqli_query($connection, $publish_post_query);
    header('location: post.php');
}




if (isset($_GET['unpublish'])) {
    $unpublish_post_id = $_GET['unpublish'];
    $unpublish_post_query = "UPDATE posts SET post_status = 'UnPublished' WHERE post_id = $unpublish_post_id";
    $unpublish_result = mysqli_query($connection, $unpublish_post_query);
    header('location: post.php');
}


?>