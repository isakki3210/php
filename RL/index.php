<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <title>Mouse Parallax Effect</title>
    <link rel="stylesheet" href="css/style.css" />
  </head>
  <body>
    <div class="container">
      <div id="parallax" class="parallax">
        <div class="img1 parallax-item" data-depth="1.2"></div>
        <div class="img2 parallax-item" data-depth="1"></div>
        <div class="img3 parallax-item" data-depth="0.7"></div>
      </div>
    </div>
    <script>
      document.addEventListener("mousemove", parallax);
      function parallax(e) {
        document.querySelectorAll(".parallax-item").forEach(function (move) {
          var moving_value = move.getAttribute("data-depth");
          var x = (window.innerWidth - e.clientX * moving_value) / 100;
          var y = (window.innerHeight - e.clientY * moving_value) / 100;
          move.style.transform = `translateX(${x}px) translateY(${y}px)`;
        });
      }
    </script>
  </body>
</html>
