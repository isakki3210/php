<?php
include 'db.php';
ob_start();
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>
    <?php
    include 'index.php';
    ?>
    <form action="Reg.php" method="post">
        <section class="vh-100">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="card text-white" style="border-radius: 1rem; background: rgba( 255, 255, 255, 0.25 );
                                                            box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
                                                            backdrop-filter: blur( 2px );
                                                            -webkit-backdrop-filter: blur( 2px );
                                                            border-radius: 10px;
                                                            border: 1px solid rgba( 255, 255, 255, 0.18 );">
                            <div class="card-body p-5 text-center">
                                <div class="mb-md-5 mt-md-4 pb-5">
                                    <h2 class="fw-bold mb-2 pb-3 text-uppercase">Register</h2>
                                    <?php

                                        if (isset($_POST['submit-r'])) {
                                            $username = mysqli_real_escape_string($connection, $_POST['username']);
                                            $email = mysqli_real_escape_string($connection, $_POST['email']);
                                            $password = mysqli_real_escape_string($connection, $_POST['password']);
                                            $password = crypt($password, '$2a$07$usesomesillystringisakkik$');
                                            $cpass = mysqli_real_escape_string($connection, $_POST['cpass']);
                                            $cpass = crypt($password, '$2a$07$usesomesillystringisakkik$');


                                            $all_query = "SELECT * FROM loginregister";
                                            $all_result = mysqli_query($connection, $all_query);
                                            while ($row = mysqli_fetch_assoc($all_result)) {
                                                $db_username = $row['username'];
                                                $db_email = $row['email'];
                                                if (!$all_result) {
                                                    die('QUERY FAILED') . mysqli_error($connection);
                                                }
                                            }

                                            if (empty($username) || empty($password) || empty($email) || empty($cpass)) {
                                                echo '<div class="alert alert-danger" role="alert">
                                            Please fill all the Fields..!!
                                            </div>';
                                            } elseif ($username == $db_username || $email == $db_email) {
                                                echo '<div class="alert alert-danger" role="alert">
                                                Email or Username is already exists Click here to <a href="login.php">Login</a>
                                            </div>';
                                            } else {
                                                $query = "INSERT INTO loginregister(username , email , pass , cpass) VALUES('$username' , '$email' , '$password' , '$cpass')";
                                                mysqli_query($connection, $query);
                                                echo '<div class="alert alert-success" role="alert" text-white>
                                            Successfully Registered Click here to <a href="login.php">Login</a>
                                            </div>';
                                            }
                                        }
                                        ?>
                                    <div class="form-outline form-white mb-4">
                                        <input type="text" id="typeEmailX" class="form-control form-control-lg"
                                            placeholder="Username" name="username" style="background: transparent;" />
                                    </div>
                                    <div class="form-outline form-white mb-4">
                                        <input type="text" id="typeEmailX" class="form-control form-control-lg"
                                            placeholder="E-mail" name="email" style="background: transparent;"/>
                                    </div>
                                    <div class="form-outline input-group form-white mb-4">
                                        <input type="password" id="typePasswordX"
                                            class="form-control form-control-lg pass" name="password"
                                            placeholder="Password" style="background: transparent;" />
                                        <span class="input-group-text" style="background: transparent;">
                                            <i class="bi bi-eye-slash" id="togglePassword"></i>
                                        </span>
                                    </div>
                                    <div class="form-outline input-group form-white mb-4">
                                        <input onkeyup="checkpass();" type="password" id="typePasswordX"
                                            class="form-control form-control-lg c-pass" name="cpass"
                                            placeholder="Confirm Password" style="background: transparent;" />
                                        <span class="input-group-text" style="background: transparent;">
                                            <i class="bi bi-eye-slash" id="toggleCPassword"></i>
                                        </span>
                                    </div>
                                    <p id="show" class="text-start"></p>
                                    <button class="btn btn-outline-light btn-lg px-5" type="submit"
                                        name="submit-r">Register Now</button>
                                </div>
                            </div>
                            <style>
                                .form-control::placeholder{
                                    color: black;
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <script src="./js/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous">
    </script>
</body>

</html>