const togglePassword = document.querySelector("#togglePassword");
const toggleCPassword = document.querySelector("#toggleCPassword");
const password = document.querySelector(".pass");
const Cpassword = document.querySelector(".c-pass");
togglePassword.addEventListener("click", function() {
  const type =
    password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
    this.classList.toggle("bi-eye");
});
toggleCPassword.addEventListener("click" , function(){
    const types =
    Cpassword.getAttribute("type") === "password" ? "text" : "password";
    Cpassword.setAttribute("type", types);
    this.classList.toggle("bi-eye");
});


function checkpass() {
    var str1 = String(document.querySelector(".pass").value);
    var str2 = String(document.querySelector(".c-pass").value);
    var fail = document.querySelector("#show");
    if (str1 == str2) {
      document.getElementById("show").innerHTML ='Password Matched';
      fail.style.color = "green";
    } else {
      document.querySelector("#show").innerHTML = 'Password not Matched';
      fail.style.color = "red";
    }
  }
  