<div class="col-lg-4">
    <!-- Search widget-->
    <div class="card mb-4">
        <div class="card-header">Search</div>
        <div class="card-body">
            <div class="input-group">
                <input class="form-control" type="text" placeholder="Enter search term..." aria-label="Enter search term..." aria-describedby="button-search" />
                <button class="btn btn-primary" id="button-search" type="button">Go!</button>
            </div>
        </div>
    </div>
    <!-- Categories widget-->
    <div class="card mb-4">
        <div class="card-header">Categories</div>
        <div class="card-body">
            <div class="row">
                <ul class="list-unstyled mb-0">
                    <?php

                    $select_all_category = "SELECT * FROM category";
                    $side_cat_result = mysqli_query($connection, $select_all_category);

                    while ($row = mysqli_fetch_assoc($side_cat_result)) {
                        $cat_title = $row['category_title'];
                        $cat_id = $row['category_id'];
                        echo "<li><a href='category.php?id={$cat_id}'>{$cat_title}</a></li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- Side widget-->
    <div class="card mb-4">
        <div class="card-header">Side Widget</div>
        <div class="card-body">You can put anything you want inside of these side widgets. They are easy to use, and feature the Bootstrap 5 card component!</div>
    </div>
</div>