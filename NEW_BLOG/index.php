<?php
include './includes/nav.php';
$connection = mysqli_connect('localhost', 'root', '', 'blog');

?>

<div class="container">
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->

            <?php
            $select_post_query = "SELECT * FROM posts";
            $select_post_result = mysqli_query($connection, $select_post_query);

            while ($row = mysqli_fetch_assoc($select_post_result)) {
                $post_title = $row['post_title'];
                $post_author = $row['post_author'];
                $post_date = $row['post_date'];
                $post_content = mb_strimwidth($row['post_content'], 0, 47, "...");
                $post_image = $row['post_image'];
                $post_tag = $row['post_tags'];
            ?>

                <div class="card mb-4">
                    <a href="#!"><img class="card-img-top" src="./images/<?php echo $post_image?>" alt="..." /></a>
                    <div class="card-body">
                        <div class="small text-muted"><?php echo $post_date ?></div>
                        <h2 class="card-title"><?php echo $post_title ?></h2>
                        <p class="card-text"><?php echo $post_content ?></p>
                        <a class="btn btn-primary" href="#!">Read more →</a>
                    </div>
                </div>
            <?php
            }
            ?>

        </div>
        <!-- Side widgets-->
        <?php
        include './includes/sidebar.php'
        ?>
    </div>
</div>
<!-- Footer-->
<?php
include './includes/footer.php';
?>