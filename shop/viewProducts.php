<section>
    <div class="container px-4 px-lg-5 mt-5">
        <h2 style="padding-bottom: 20px;">
            GROUP OF ITEMS
        </h2>
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">


            <?php
            $select_ViewProducts = "SELECT DISTINCT dress_type,common_dress FROM popular_item";
            $select_ViewProducts_query = mysqli_query($connection, $select_ViewProducts);
            while ($row = mysqli_fetch_assoc($select_ViewProducts_query)) {
                $dress_type = $row['dress_type'];
                $common_dress = $row['common_dress'];
            ?>

                <div class="col mb-5">
                    <div class="card h-100">
                        <img class="card-img-top" src="./img/<?php echo $common_dress?>" alt="..." height="400px"/>
                        <div class="card-body p-4">
                            <div class="text-center">
                                <h5 class="fw-bolder text-capitalize"><?php echo $dress_type ?></h5>
                                $40.00 - $300.00
                            </div>
                        </div>
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="./viewMore.php?dress_type=<?php echo $dress_type?>">View More</a></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>