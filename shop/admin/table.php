<?php
include 'includes/db.php';
include 'includes/head.php';
include 'includes/sidebar.php';
include 'includes/nav.php';
?>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">ALL ITEMS</h1>
    <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                        For more information about DataTables, please visit the <a target="_blank"
                            href="https://datatables.net">official DataTables documentation</a>.</p> -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All items</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Dress Name</th>
                            <th>Dress Type</th>
                            <th>Price</th>
                            <th>Rating</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $select_table = "SELECT * FROM popular_item";
                        $select_table_query = mysqli_query($connection, $select_table);

                        while ($row = mysqli_fetch_assoc($select_table_query)) {
                            $id = $row['id'];
                            $dress_name = $row['pop_name'];
                            $dress_type = $row['dress_type'];
                            $rating = $row['pop_star'];
                            $dress_price = $row['pop_price'];
                        ?>

                            <tr>
                                <td><?php echo $id ?></td>
                                <td><?php echo $dress_name ?></td>
                                <td><?php echo $dress_type ?></td>
                                <td><?php echo $dress_price ?></td>
                                <td>
                                    <div class="d-flex justify-content-center small text-dark mb-2">
                                        <?php
                                        for ($i = 0; $i < $rating; $i++) {
                                        ?>
                                            <div class="bi-star-fill"></div>
                                            <?php }
                                        if ($rating < 5) {
                                            for ($bal = $rating; $bal < 5; $bal++) {
                                            ?>
                                                <div class="bi-star"></div>
                                        <?php }
                                        }
                                        ?>
                                        (<?php echo $rating ?>)
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/datatables-demo.js"></script>

</body>

</html>