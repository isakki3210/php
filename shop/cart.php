<?php
include './includes/head.php';
include './includes/nav.php';
include './includes/db.php';

$pop_id = $_GET['pop_id'];

$select_item = "SELECT * FROM popular_item WHERE id = $pop_id";
$select_item_query = mysqli_query($connection, $select_item);

while ($row = mysqli_fetch_assoc($select_item_query)) {
    $pop_name = $row['pop_name'];
    $pop_price = $row['pop_price'];
    $pop_type = $row['dress_type'];
    $pop_star = $row['pop_star'];
    $pop_img = $row['pop_img'];
}

?>


<section class="py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="row gx-4 gx-lg-5 align-items-center">
            <div class="col-md-6 w-50"><img class="card-img-top" src="./img/<?php echo $pop_img ?>" alt="..." style="height:100%; width:100%" /></div>
            <div class="col-md-6">
                <div class="small mb-1">SKU: BST-498</div>
                <h1 class="display-5 fw-bolder"><?php echo $pop_name ?></h1>
                <div class="fs-5 mb-5">
                    <span>$<?php echo $pop_price ?></span>
                </div>
                <div class="fs-5 mb-5">
                    <div class="d-flex justify-content small text-warning mb-2">
                        <?php
                        for ($i = 0; $i < $pop_star; $i++) {
                        ?>
                            <div class="bi-star-fill"></div>
                            <?php }
                        if ($pop_star < 5) {
                            for ($bal = $pop_star; $bal < 5; $bal++) {
                            ?>
                                <div class="bi-star"></div>
                        <?php }
                        }
                        ?>
                    </div>
                </div>
                <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium at dolorem quidem modi. Nam sequi consequatur obcaecati excepturi alias magni, accusamus eius blanditiis delectus ipsam minima ea iste laborum vero?</p>


                <?php
                if (isset($_POST['add_cart'])) {
                    $sel = "SELECT * FROM popular_item WHERE id = $pop_id";
                    $res=mysqli_query($connection , $sel);
                        while($row = mysqli_fetch_assoc($res)){
                            $already_cart = $row['cart'];
                        }
                    $quantity  = $_POST['quantity'];
                    $quantity += $already_cart;
                    $update_cart = "UPDATE popular_item SET cart = $quantity WHERE id=$pop_id";
                    $update_res = mysqli_query($connection, $update_cart);
                }
                ?>

                <form action="" method="post">
                    <div class="d-flex">
                        <input class="form-control text-center me-3" id="inputQuantity" type="num" value="1" style="max-width: 3rem" name="quantity" />
                        <button class="btn btn-outline-dark flex-shrink-0 m-1" type="submit" name="add_cart">
                            <i class="bi-cart-fill me-1"></i>
                            Add to cart
                        </button>
                        <button class="btn btn-outline-dark flex-shrink-0 m-1" type="submit">
                            <i class="bi-cart-fill me-1"></i>
                            Buy Now
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php
include './related_items.php';
include './includes/footer.php';
?>