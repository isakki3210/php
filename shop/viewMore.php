<?php
include './includes/head.php';
include './includes/nav.php';
include './includes/db.php';
$dress_type = $_GET['dress_type'];
?>

<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <h2 style="padding-bottom: 20px;" class="text-capitalize">
            View More - <?php echo $dress_type ?>
        </h2>
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

            <?php
            $select_view_more = "SELECT * FROM popular_item WHERE dress_type = '$dress_type'";
            $view_more_res = mysqli_query($connection, $select_view_more);

            while ($row = mysqli_fetch_assoc($view_more_res)) {
                $pop_name  =   $row['pop_name'];
                $pop_img  =   $row['pop_img'];
                $pop_price  =   $row['pop_price'];
                $pop_id = $row['id'];
                $pop_star  =   $row['pop_star'];
            ?>

                <div class="col mb-5">
                    <div class="card h-100">
                        <img src="./img/<?php echo $pop_img ?>" class="card-img-top" alt="..." style="height:300px">
                        <div class="card-body p-4">
                            <div class="text-center">
                                <h5 class="fw-bolder p-1"><?php echo $pop_name ?></h5>
                                <div class="d-flex justify-content-center small text-warning mb-2">
                                    <?php
                                    for ($i = 0; $i < $pop_star; $i++) {
                                    ?>
                                        <div class="bi-star-fill"></div>
                                        <?php }
                                    if ($pop_star < 5) {
                                        for ($bal = $pop_star; $bal < 5; $bal++) {
                                        ?>
                                            <div class="bi-star"></div>
                                    <?php }
                                    }
                                    ?>
                                </div>
                                <div>
                                    $<?php echo $pop_price ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent d-flex justify-content-center">
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto m-1" href="./cart.php?pop_id=<?php echo $pop_id; ?>">Add to cart</a></div>
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="./cart.php?pop_id=<?php echo $pop_id; ?>">Buy Now</a></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php
include './includes/footer.php';
?>