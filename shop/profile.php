<?php ob_start();
include 'includes/db.php';
include 'includes/head.php';
include 'includes/nav.php';
?>

<div class="container-fluid">


    <?php

    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    } else {
        $page = '';
    }
    switch ($page) {
        case 'edit_profile';
            include 'includes/edit_profile.php';
            break;

        default:
            include 'includes/viewProfile.php';
            break;
    }
    ?>


</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
</body>

</html>