<?php

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] !== 'user') {
        session_destroy();
        header('location: ./login.php');
    }
} else {
    session_destroy();
    header('location: ./login.php');
}
include './includes/db.php';
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="#!">ShopNew</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link active mt-1" aria-current="page" href="#!">Home</a></li>
            </ul>
            <?php
            $select_cart = "SELECT * FROM popular_item";
            $cart_query = mysqli_query($connection, $select_cart);
            $sum = 0;
            while ($row  = mysqli_fetch_assoc($cart_query)) {
                $cart_quantity = $row['cart'];
                $sum += $cart_quantity;
            }

            ?>

            <form class="d-flex float-end">
                <button class="btn btn-outline-dark">
                    <i class="bi-cart-fill me-1"></i>
                    Cart
                    <span class="badge bg-dark text-white ms-1 rounded-pill"><?php echo $sum ?></span>
                </button>

            </form>
            <ul class="navbar-nav d-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-dark " id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="profile.php">Profile</a></li>
                        <li><a class="dropdown-item" href="logout.php">Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>